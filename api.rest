GET http://localhost:3000/api/products
###
GET http://localhost:3000/api/products/department/uno
###
GET http://localhost:3000/api/products/66807630971ed866d40838e0
###
GET http://localhost:3000/api/products/66807651971ed866d40838e
###
POST http://localhost:3000/api/products
Content-Type: application/json

{
  "name": "ZZZ",
  "description": "dos",
  "price": 690,
  "stock": 0,
  "department": "departamento"
}
###
PUT http://localhost:3000/api/products/66807630971ed866d40838e0
Content-Type: application/json

{
  "name": "-------------------",
  "description": "________________",
  "price": 690,
  "stock": 8,
  "department": "departamento",
  "available": true
}
###
DELETE http://localhost:3000/api/products/668099741d8b67e5bb98f7d7
##############
# Users
GET http://localhost:3000/api/users
###
POST http://localhost:3000/api/users
Content-Type: application/json

{
  "username": "lookus",
  "email": "bienyvos",
  "pass": "secreto"
}
###
PUT http://localhost:3000/api/users/add_product/6680a6cdf06d7225a6900297
Content-Type: application/json

{
  "productId": "66809e8860cbcb15798eeaeb"
}
###