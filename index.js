const http = require('http');
const app = require('./src/app');
// conf .env
require('dotenv').config();

//conf DB
require('./src/config/db');

const server = http.createServer(app);

const PORT = process.env.PORT || 3000;
server.listen(PORT);

server.on('listening', () => {
  console.log('server listening o port', PORT);
})