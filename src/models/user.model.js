const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  username: String,
  email: String,
  pass: String,
  cart: [{type: Schema.ObjectId, ref: 'product'}]
},{
  timestamps: true,
  versionKey: false
});

module.exports = model('user', userSchema);