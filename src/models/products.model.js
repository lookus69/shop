const {Schema, model} = require('mongoose');

const productSchema = new Schema({
  name: String,
  description: String,
  price: Number,
  stock: Number,
  department: String
  // available: Boolean
}, {
  timestamps: true, 
  versionKey: false,
  toJSON: {virtuals: true},
  toObject: {virtuals: true}
});

productSchema.virtual('priceTax').get(function () {
  return this.price * 1.21;
});
productSchema.virtual('available').get(function () {
  // console.log('stock:', this.stock);
  return this.stock > 0 ? true : false;
});

productSchema.statics.findByDepartment = function (dep){
  // console.log('dept:', dep);
  return model('product').find({department: dep});
};

module.exports = model('product', productSchema); 