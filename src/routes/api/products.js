const router = require('express').Router();
const pc = require('../../controllers/products.controller');

router.get('/', pc.getAllProducts);
router.get('/department/:department', pc.findProductsByDepartment);
router.get('/:productId', pc.getProductById);
router.post('/', pc.createProduct);
router.put('/:productId', pc.updateProduct);
router.delete('/:productId', pc.deleteProduct);


module.exports = router;