const router = require('express').Router();
const uc = require('../../controllers/users.controller');

router.post('/', uc.createUser);
router.put('/add_product/:userId', uc.addProduct);
router.get('/', uc.getAllUsers);


module.exports = router;