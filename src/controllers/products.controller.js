const Product = require('../models/products.model');

const getAllProducts = async (req, res) => {
  const products = await Product.find();
  res.json(products);

};
const getProductById = (req, res) => {
  const {productId} = req.params;
  console.log('get product by id', productId);
  Product.findById(productId).then((ok) => {
      console.log('ok:',ok);
      res.json(ok);
    }, (ko) => {
      console.log('ko:', ko.message);
      res.json(ko.message);
    }).catch((e) => {
    console.log('catch:', e.message);
    res.json({error: e.message});
  });
  // console.log(product);
  // res.json("product");
};
const createProduct = async (req, res) => {
  console.log(req.body);
  const newProduct = await Product.create(req.body);
  res.json(newProduct);
};

const updateProduct = (req, res) => {
  const {productId} = req.params;
  Product.findByIdAndUpdate(productId, req.body, {new: true}).then((ok) => {
    if(ok)
      res.json(ok);
    else
      res.json({resultado: "id no encontrada"});
  }, (e) => {
    res.json(e.message);
  });
};

const deleteProduct = (req, res) => {
  const {productId} = req.params;
  Product.findByIdAndDelete(productId).then(ok =>{
    if(ok)
      res.json({eliminado: ok.name});
    else
      res.json({eliminado: false, message: "no encontrado"});
  }, e => {
    res.json(e.message);
  });
};

const findProductsByDepartment = (req, res) => {
  const {department} = req.params;
  Product.findByDepartment(department).then((prods) => {
    res.json(prods);
  }, (e) => {
    res.json(e.message);
  });
};

module.exports = {
  getAllProducts,
  getProductById,
  createProduct,
  updateProduct,
  deleteProduct,
  findProductsByDepartment
}