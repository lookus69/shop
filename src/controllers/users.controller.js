const User = require('../models/user.model');

const createUser = (req, res) => {
  User.create(req.body).then((u) => {
    res.json(u);
  }, (e) => {
    res.json(e.message);
  });
};//createUser

const addProduct = (req, res) => {
  const {userId} = req.params;
  User.findById(userId).then(user => {
    user.cart.push(req.body.productId);
    user.save().then( u => {
      res.json(u);
    }, e => res.json(e.message));
  }, e => {
    res.json(e.message);
  });
};//addProduct

const getAllUsers = (req, res) => {
  User.find().populate('cart').then(u => {
    res.json(u);
  }, e => res.json(e.message));
};//getAllUsers

module.exports = {
  createUser,
  addProduct,
  getAllUsers
}